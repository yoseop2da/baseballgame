//
//  SettingManager.m
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 5..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import "SettingManager.h"
@interface SettingManager()
{
    int _selectedSound;
    int _selectedColor;
}
@end

@implementation SettingManager

+ (instancetype)sharedInstance {
    static SettingManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [SettingManager new];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    if (self = [super init]) {
        _isOnBGSound = ![[NSUserDefaults standardUserDefaults] boolForKey:@"ISOFF_BGSOUND"];
        _isOnEffectSound = ![[NSUserDefaults standardUserDefaults] boolForKey:@"ISOFF_EFFECTSOUND"];
        _is4Number = [[NSUserDefaults standardUserDefaults] boolForKey:@"IS_FOUR"];

        NSNumber *sound = [[NSUserDefaults standardUserDefaults] objectForKey:@"SOUND_INDEX"];
        if (!sound) {
            _selectedSound = 0;
        }else{
            _selectedSound = [sound intValue];
        }
        
        NSNumber *color = [[NSUserDefaults standardUserDefaults] objectForKey:@"COLOR_INDEX"];
        if (!color) {
            _selectedColor = 10;
        }else{
            _selectedColor = [color intValue];
        }
        
    }
    return self;
}

-(void)setIsOnBGSound:(BOOL)isOnBGSound
{
    _isOnBGSound = isOnBGSound;
    [[NSUserDefaults standardUserDefaults] setBool:!_isOnBGSound forKey:@"ISOFF_BGSOUND"];

    if (isOnBGSound) {
        [SharedAppDelegate playBGsound];
    }else{
        [SharedAppDelegate stopBGSound];
    }
}

- (void)setIsOnEffectSound:(BOOL)isOnEffectSound
{
    _isOnEffectSound = isOnEffectSound;
    [[NSUserDefaults standardUserDefaults] setBool:!_isOnEffectSound forKey:@"ISOFF_EFFECTSOUND"];
}

- (void)setIs4Number:(BOOL)is4Number
{
    _is4Number = is4Number;
    [SharedAppDelegate.mainViewController initialzeRandomNumber];
    [[NSUserDefaults standardUserDefaults] setBool:_is4Number forKey:@"IS_FOUR"];
}

- (int)MaxNumber
{
    return _is4Number? 4:3;
}

- (void)setSelectedSound:(int)selectedSound
{
    _selectedSound = selectedSound;
    [[NSUserDefaults standardUserDefaults] setObject:@(selectedSound) forKey:@"SOUND_INDEX"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (int)selectedSound
{
    return _selectedSound;
}

- (NSString *)bgName
{
    NSArray *soundNames = @[@"defaultSound",@"AuldLangSyne",@"Biggie",@"Brahms_Lullaby",@"Dutty",@"Give",@"Haus_Guest",@"Hey_Girl",@"Killing_With_Kindness",@"Local_Saloon",@"London_Bridge",@"Lucky_Shot",@"Open_Sea_Morning",@"Orange Blanket",@"Otis_McMusic",@"Outlet",@"Raindrops",@"Run-EM",@"Spring_In_My_Step",@"Take_That_Back",@"The_Big_Guns",@"The_Big_Score",@"The_Bluest_Star",@"The_Farmer_In_The_Dell",@"The_Wrong_Way",@"Tied_Up",@"Together_With_You",@"Wigs"];
//    ,@"bg02",@"bg03",@"bg04"];
    return soundNames[_selectedSound];
}

-(void)setSelectedColor:(int)selectedColor
{
    _selectedColor = selectedColor;
    [[NSUserDefaults standardUserDefaults] setObject:@(selectedColor) forKey:@"COLOR_INDEX"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (int)selectedColor
{
    return _selectedColor;
}

- (UIColor *)currentColor
{
    return self.colorArray[_selectedColor];
}

- (NSArray *)colorArray
{
    return @[CustomWhite,CustomRed,CustomGreen,CustomTree,CustomGray,CustomPurple,CustomPurple2,CustomSky,CustomPink,CustomBlue,CustomOrange];
}
@end

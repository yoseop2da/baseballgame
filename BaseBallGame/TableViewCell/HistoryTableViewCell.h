//
//  HistoryTableViewCell.h
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 3..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryModel.h"
@interface HistoryTableViewCell : UITableViewCell
{
    IBOutlet UILabel *numberLabel;
    IBOutlet UILabel *firstLabel;
    IBOutlet UILabel *secondLabel;
}

- (void)setHistoryValue:(HistoryModel *)historyValue;

@end

//
//  SettingTableViewCell.m
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 5..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import "SettingTableViewCell.h"

@implementation SettingTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTitle:(NSString *)title;
{
    titleTextLabel.text = title;
    
    BOOL isON = NO;
    if (self.tag == 0) {
        isON = [SettingManager sharedInstance].isOnBGSound;
    }else if (self.tag == 1) {
        isON = [SettingManager sharedInstance].isOnEffectSound;
    }else if (self.tag == 2) {
        isON = [SettingManager sharedInstance].is4Number;
    }else if (self.tag == 3) {
        
    }else if (self.tag == 4) {
        
    }
    settingSwitch.on = isON;
}

- (IBAction)switchChanged:(UISwitch *)sender
{
    if (self.tag == 0) {
        [SettingManager sharedInstance].isOnBGSound = sender.on;
    }else if (self.tag == 1) {
        [SettingManager sharedInstance].isOnEffectSound = sender.on;
    }else if (self.tag == 2) {
        [SettingManager sharedInstance].is4Number = sender.on;
    }else if (self.tag == 3) {
        
    }else if (self.tag == 4) {
        
    }
}

- (void)hideSwitch
{
    settingSwitch.hidden = YES;
}
@end

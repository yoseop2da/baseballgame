//
//  HistoryTableViewCell.m
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 3..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import "HistoryTableViewCell.h"

@implementation HistoryTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setHistoryValue:(HistoryModel *)historyValue;
{
    numberLabel.text = [NSString stringWithFormat:@"%02d",(int)self.tag+1];
    firstLabel.text = historyValue.numberValue;
    
    NSString *matchedString = @"";
    if (historyValue.strike > 0) {
        if (historyValue.strike == [SettingManager sharedInstance].MaxNumber) {
            matchedString = @"Home Run";
        }else{
            if (historyValue.ball > 0) {
                matchedString = [NSString stringWithFormat:@"%dS %dB",historyValue.strike,historyValue.ball];
            }else{
                matchedString = [NSString stringWithFormat:@"%dS",historyValue.strike];
            }
        }
    }else{
        if (historyValue.ball > 0) {
            matchedString = [NSString stringWithFormat:@"%dB",historyValue.ball];
        }else{
            matchedString = @"Out";
        }
    }
    secondLabel.text = matchedString;
}
@end

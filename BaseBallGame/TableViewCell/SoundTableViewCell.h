//
//  SoundTableViewCell.h
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 6..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SoundTableViewCell : UITableViewCell
{
    IBOutlet UILabel *soundNameLabel;
    IBOutlet UIImageView *checkMark;
}

- (void)setSound:(NSString *)sound check:(BOOL)isCheck;
@end

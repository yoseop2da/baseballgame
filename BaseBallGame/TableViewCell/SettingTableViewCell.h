//
//  SettingTableViewCell.h
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 5..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableViewCell : UITableViewCell
{
    IBOutlet UILabel *titleTextLabel;
    IBOutlet UISwitch *settingSwitch;
}

- (void)setTitle:(NSString *)title;
- (void)hideSwitch;

@end

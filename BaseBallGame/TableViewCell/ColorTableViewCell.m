//
//  ColorTableViewCell.m
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 10..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import "ColorTableViewCell.h"

@interface ColorTableViewCell()
{
    IBOutletCollection(UIButton) NSArray *colorButtons;
}
@end

@implementation ColorTableViewCell

- (void)awakeFromNib {
    [self setColors];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setColors
{
    for (UIButton *button in colorButtons) {
        UIColor *color = [SettingManager sharedInstance].colorArray[button.tag];
        [button setBackgroundColor:color];
        button.layer.cornerRadius = 5.f;
        [button addTarget:self action:@selector(colorButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
        
        if (button.tag == [SettingManager sharedInstance].selectedColor) {
            button.selected = YES;
        }else{
            button.selected = NO;
        }
    }
}

- (void)colorButtonTouched:(UIButton *)button
{
    [SettingManager sharedInstance].selectedColor = (int)button.tag;
    button.selected = YES;
    [self setColors];
    [self postColorChangeNotification];
}

- (void)postColorChangeNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ColorChanged" object:nil];
}

@end

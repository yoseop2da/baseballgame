
//
//  SoundTableViewCell.m
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 6..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import "SoundTableViewCell.h"

@implementation SoundTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setSound:(NSString *)sound check:(BOOL)isCheck
{
    soundNameLabel.text = sound;
    checkMark.hidden = !isCheck;
}

@end

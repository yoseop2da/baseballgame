//
//  SettingManager.h
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 5..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingManager : NSObject

+ (instancetype)sharedInstance;

@property (nonatomic) BOOL isOnBGSound;
@property (nonatomic) BOOL isOnEffectSound;
@property (nonatomic) BOOL is4Number;

@property (nonatomic) int selectedSound;
@property (nonatomic) int selectedColor;

- (int)MaxNumber;
- (NSString *)bgName;
- (NSArray *)colorArray;
- (UIColor *)currentColor;
@end

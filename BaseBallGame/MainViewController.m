//
//  ViewController.m
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 3..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import "MainViewController.h"
#import "SettingViewController.h"
#import "GuideViewController.h"
#import "CommonNavigationView.h"

#import "HistoryTableViewCell.h"
#import "HistoryModel.h"

#import "HintView.h"
#import "SuccessView.h"
@import GoogleMobileAds;

@interface MainViewController ()<UITableViewDataSource, UITableViewDelegate, GADInterstitialDelegate>
{
    //==================================
    IBOutlet CommonNavigationView *navigationView;
    IBOutlet UIButton *settingButton;
    IBOutlet UILabel *titleLabel;
    IBOutlet UIButton *hintButton;
    //==================================

    IBOutlet UITableView *historyTableView;
    IBOutlet UITextField *inputTextField;
    IBOutlet UIButton *topBatButton;
    IBOutletCollection(UIButton) NSArray *numberButtons;
    IBOutlet UIImageView *player;

    
    NSMutableArray *_resultNumbers;
    NSMutableArray *_historyArray;
    
    HintView *_hintView;
    NSMutableArray *_outStringArray;
    NSMutableArray *_tempArray;


    SuccessView *_successView;
    BOOL _iaHintTouchValid;
    int _ballCount;
    
    IBOutlet GADBannerView *bannerView;
    GADInterstitial *interstitial;
    
}
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _iaHintTouchValid = YES;
    SharedAppDelegate.mainViewController = self;
    
    [self initialzeRandomNumber];
    
    inputTextField.placeholder = @"숫자를 입력후 배트를 눌러주세요.";
    inputTextField.layer.masksToBounds = NO;
    inputTextField.layer.shadowColor = [UIColor blackColor].CGColor;
    inputTextField.layer.shadowOpacity = 0.8;
    inputTextField.layer.shadowRadius = 1;
    inputTextField.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    inputTextField.layer.sublayerTransform = CATransform3DMakeTranslation(15, 0, 0);
    
    [SharedAppDelegate playBGsound];
    
    [self playerAnimation];
    [self ballAnimating];
    
    topBatButton.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshBackgroundColor) name:@"ColorChanged" object:nil];
    [self refreshBackgroundColor];
    
    [self addAdMob];
}

- (void)showGuideView
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isUserWatchedGuideView"])
    {
        GuideViewController *guideViewController = [SharedAppDelegate.mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([GuideViewController class])];
        [self presentViewController:guideViewController animated:YES completion:nil];
    }
}

- (void)refreshBackgroundColor
{
    self.view.backgroundColor = [SettingManager sharedInstance].currentColor;
}

- (void)addAdMob
{
    bannerView.adUnitID = @"ca-app-pub-2783299302427084/2061087251";
    bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
    
    interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-2783299302427084/4468203254"];
    interstitial.delegate = self;
    [interstitial loadRequest:request];
}

- (void)loadFullAd
{
    if ([interstitial isReady]) {
        [interstitial presentFromRootViewController:self];
    }
}

- (void)initialize
{
    inputTextField.text = @"";
}

#pragma mark - Button Actions
- (IBAction)numberButtonTouched:(UIButton *)numButton
{
    NSString *beforeString = @"";
    beforeString = [beforeString stringByAppendingString:inputTextField.text];
    
    if (numButton.tag == 0) {
        // Delete
        if (beforeString.length > 0) {
            beforeString = [beforeString substringWithRange:NSMakeRange(0, beforeString.length-1)];
        }
    }else{
        NSString *string = [@(numButton.tag) stringValue];
        beforeString = [beforeString stringByAppendingString:string];
        [SharedAppDelegate playButtonSound];
    }
    
    if (beforeString.length > [SettingManager sharedInstance].MaxNumber) {
        return;
    }

    NSLog(@"beforeString : %@",beforeString);
    inputTextField.text = beforeString;
    
}

- (IBAction)settingsButtonTouched:(id)sender
{
    SettingViewController *settingViewController = [SharedAppDelegate.mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([SettingViewController class])];
    [self.navigationController pushViewController:settingViewController animated:YES];
}

- (IBAction)hintButtonTouched:(id)sender
{
    if (!_iaHintTouchValid) {
        return;
    }
    
    [SharedAppDelegate playBallPassSound];
    _iaHintTouchValid = NO;
    [inputTextField resignFirstResponder];
    
    if (!_hintView) {
        _hintView = [[HintView alloc] initWithHintArray:_resultNumbers];
        [self.view addSubview:_hintView];
        [SharedAppDelegate.window bringSubviewToFront:_hintView];
    }
    [_hintView showHintView];
    
    
    [self performSelector:@selector(touchValid) withObject:nil afterDelay:.5f];
}

- (IBAction)doneButtonTouched:(id)sender
{
    if ([self isValid]) {
        [SharedAppDelegate playBatSound];
        [self reloadTableViewData];
        [self initialize];
    }
}

#pragma mark - 
- (void)reloadTableViewData
{
    int countOfStrike = 0;
    int countOfBall = 0;
    
    for (int i = 0; i < [SettingManager sharedInstance].MaxNumber ; i++) {
        NSString *string = [inputTextField.text substringWithRange:NSMakeRange(i, 1)];
        int index = 0;
        for (NSString *rString in _resultNumbers) {
            if ([string isEqualToString:rString]) {
                if (i == index) {
                    countOfStrike++;
                }else{
                    countOfBall++;
                }
            }
            index++;
        }
    }
    HistoryModel *historyModel = [HistoryModel new];
    historyModel.numberValue = inputTextField.text;
    
    if (countOfStrike > 0) {
        if (countOfStrike == [SettingManager sharedInstance].MaxNumber) {
            _successView = [SuccessView new];
            [self.view addSubview:_successView];
            [_successView animating];
        }
        historyModel.strike = countOfStrike;
    }
    if (countOfBall > 0) {
        historyModel.ball = countOfBall;
    }
    if (countOfStrike == 0 && countOfBall == 0){
        [SharedAppDelegate playOutSound];
        for (int i = 0; i < [SettingManager sharedInstance].MaxNumber ; i++) {
            NSString *string = [inputTextField.text substringWithRange:NSMakeRange(i, 1)];
            if ([_outStringArray containsObject:string]) {
                
            }else{
                [_outStringArray addObject:string];
            }
        }
        [self checkNumberString];
    }
    
    if (_historyArray.count > 0) {
        [_historyArray insertObject:historyModel atIndex:0];
    }else{
        [_historyArray addObject:historyModel];
    }

    NSLog(@"S: %d, B: %d",countOfStrike,countOfBall);
    
    [historyTableView reloadData];
}

- (void)checkNumberString
{
    for (NSString *string in _outStringArray) {
        for (UIButton *button in numberButtons) {
            if (button.tag == [string integerValue]) {
                button.backgroundColor = [UIColor darkGrayColor];
            }
        }
    }
}

- (BOOL)isValid
{
    BOOL isValid;
    if ((int)inputTextField.text.length < [SettingManager sharedInstance].MaxNumber) {
        [SharedAppDelegate showGlobalProgressHUDWithTitle:[NSString stringWithFormat:@"총 %d개의 수를 입력해주세요.",[SettingManager sharedInstance].MaxNumber]];
        isValid = NO;
    }else{
        NSMutableArray *tempArray = [NSMutableArray array];
        isValid = YES;
        for (int i = 0; i < [SettingManager sharedInstance].MaxNumber ; i++) {
            NSString *string = [inputTextField.text substringWithRange:NSMakeRange(i, 1)];
            if ([tempArray containsObject:string]) {
                [SharedAppDelegate showGlobalProgressHUDWithTitle:@"중복되는 숫자가 있습니다."];
                isValid = NO;
            }else{
                [tempArray addObject:string];
            }
        }
    }
    return isValid;
}

#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _historyArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([HistoryTableViewCell class])];
    cell.tag = _historyArray.count - indexPath.row -1;
    HistoryModel *historyModel = _historyArray[indexPath.row];
    [cell setHistoryValue:historyModel];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    if(iOSDeviceScreenSize.height <= 568){
        return 35;
    }else{
        return 50;
    }
}

- (void)touchValid
{
    _iaHintTouchValid = YES;
}

#pragma mark - Create Random Number

- (void)initialzeRandomNumber
{
    _outStringArray = [NSMutableArray array];
    _historyArray = [NSMutableArray array];
    _resultNumbers = [NSMutableArray array];
    [self resultArray];
    NSLog(@"resultArray : %@",_resultNumbers);
    
    _hintView = [[HintView alloc] initWithHintArray:_resultNumbers];
    [self.view addSubview:_hintView];
    
    [historyTableView reloadData];
    [self checkNumberString];
    
    for (UIButton *button in numberButtons) {
        button.layer.cornerRadius = 5.f;
        button.layer.borderWidth = 0.5f;
        
        button.layer.masksToBounds = NO;
        button.layer.shadowColor = [UIColor blackColor].CGColor;
        button.layer.shadowOpacity = 0.8;
        button.layer.shadowRadius = 1;
        button.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
        
        
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        if (button.tag == 0) {
            button.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1.f];
            [button setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        }else{
            button.backgroundColor = [UIColor whiteColor];
            [button setTitle:[@(button.tag) stringValue] forState:UIControlStateNormal];
        }
    }
}
                    
- (NSString *)randomNumberString
{
    int value = (int)arc4random_uniform(10)+1;
    if (value == 10) {
        value -= 1;
    }
    NSString *string = [NSString stringWithFormat:@"%d",value];
    
    return string;
}
     
- (void)resultArray
{
    if (_resultNumbers.count == [SettingManager sharedInstance].MaxNumber) return;
    NSString *ranString = [self randomNumberString];
    
    if ([_resultNumbers containsObject:ranString]) {
        [self resultArray];
    }else{
        [_resultNumbers addObject:ranString];
        [self resultArray];
    }
}

- (void)playerAnimation
{
    int value = (int)arc4random_uniform(4);
    float x,y,z;
    x = 0.f;
    y = 0.f;
    z = 0.f;
    switch (value%4) {
        case 0:
        {
            x = 1.f;
        }
            break;
        case 1:
        {
            y = 1.f;
        }
            break;
        case 2:
        {
            x = 1.f;
            y = 1.f;
        }
            break;
        case 3:
        {
            y = 1.f;
            z = 1.f;
        }
            break;
            
        default:
            break;
    }
    NSLog(@"[%d] %f %f %f",value%4,x,y,z);
    
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m34 = 1.0 / -1000.0;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, M_PI * 1.0, x, y, z);
    
    CATransform3D endRotationAndPerspectiveTransform = CATransform3DIdentity;
    endRotationAndPerspectiveTransform.m34 = 1.0 / -1000.0;
    endRotationAndPerspectiveTransform = CATransform3DRotate(endRotationAndPerspectiveTransform, 0, x, y, z);
    
    [UIView animateWithDuration:1.f animations:^{
        player.layer.transform = rotationAndPerspectiveTransform;
    } completion:^(BOOL finished){
        [UIView animateWithDuration:1.f animations:^{
            player.layer.transform = endRotationAndPerspectiveTransform;
        } completion:nil];
    }];
}

- (void)ballAnimating
{
    UIImage *image = [UIImage imageNamed:@"ball"];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    imageView.image = image;
    [self.view addSubview:imageView];
    [imageView sendSubviewToBack:topBatButton];
    
    if (!_tempArray) {
        _tempArray = [NSMutableArray array];
    }
    [_tempArray addObject:imageView];
    
    int xValue = (int)arc4random_uniform([UIScreen mainScreen].bounds.size.width);
    
    CGPoint oldPoint = self.view.center;
    oldPoint.x = xValue;
    oldPoint.y = self.view.center.x - 200;
    imageView.center = oldPoint;
    [self runSpinAnimationOnView:imageView duration:1.f rotations:1.f repeat:1];
    [UIView animateWithDuration:0.2f animations:^{
        CGPoint oldPoint = self.view.center;
        oldPoint.x = [UIScreen mainScreen].bounds.size.width - 40;
        oldPoint.y = [UIScreen mainScreen].bounds.size.height - 160 - 50;
        imageView.center = oldPoint;
    }completion:^(BOOL finished) {
        if (_ballCount < 10) {
            [self ballAnimating];
        }else{
            topBatButton.hidden = NO;
            for (UIView *v in _tempArray) {
                [v removeFromSuperview];
            }
            
            [self showGuideView];
        }
        
        _ballCount++;
    }];
}

- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

@end

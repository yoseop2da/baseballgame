
//
//  GuideViewController.m
//  BaseBallGame
//
//  Created by parkyoseop on 2015. 11. 23..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import "GuideViewController.h"

@interface GuideViewController ()
{
    IBOutlet UITextView *guideTextView;
}
@end

@implementation GuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshBackgroundColor) name:@"ColorChanged" object:nil];
    [self refreshBackgroundColor];
}

- (void)refreshBackgroundColor
{
    self.view.backgroundColor = [SettingManager sharedInstance].currentColor;
    guideTextView.backgroundColor = [SettingManager sharedInstance].currentColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)okButtonTouched:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isUserWatchedGuideView"];
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end

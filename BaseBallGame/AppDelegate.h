//
//  AppDelegate.h
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 3..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIStoryboard *mainStoryboard;

@property (strong) MainViewController *mainViewController;

- (MBProgressHUD *)showGlobalProgressHUDWithTitle:(NSString *)title;
- (void)dismissGlobalHUD;


- (void)playBGsound;
- (void)stopBGSound;
- (void)stopEffectSound;

- (void)playBatSound;
- (void)playSuccessSound;
- (void)playOutSound;
- (void)playBallPassSound;

- (void)playButtonSound;
@end


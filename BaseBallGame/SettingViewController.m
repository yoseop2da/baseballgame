//
//  SettingViewController.m
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 5..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import "SettingViewController.h"
#import "CommonNavigationView.h"
#import "SettingTableViewCell.h"
#import "SoundTableViewCell.h"
#import "ColorTableViewCell.h"
#import "GuideViewController.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface SettingViewController ()<UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>
{
    //==================================
    IBOutlet CommonNavigationView *navigationView;
    IBOutlet UIButton *backButton;
    IBOutlet UILabel *titleLabel;
    //==================================
    
    IBOutlet UITableView *settingTableView;
    
    NSArray *_settingArray;
    NSArray *_soundArray;
}
@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _settingArray = @[@"배경음악",@"효과음",@"4자리 숫자야구"];
    _soundArray = @[@"기본음",@"AuldLangSyne",@"Biggie",@"Brahms Lullaby",@"Dutty",@"Give",@"Haus Guest",@"Hey Girl",@"Killing With Kindness",@"Local Saloon",@"London Bridge",@"Lucky Shot",@"Open Sea Morning",@"Orange Blanket",@"Otis McMusic",@"Outlet",@"Raindrops",@"Run-EM",@"Spring In My Step",@"Take That Back",@"The Big Guns",@"The Big Score",@"The Bluest Star",@"The Farmer In The Dell",@"The Wrong Way",@"Tied Up",@"Together With You",@"Wigs"];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshBackgroundColor) name:@"ColorChanged" object:nil];
    [self refreshBackgroundColor];
}

- (void)refreshBackgroundColor
{
    self.view.backgroundColor = [SettingManager sharedInstance].currentColor;
}

#pragma mark - Button Actions
- (IBAction)backButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0 || section == 2 || section == 4) {
        return 1;
    }else if (section == 1) {
        return _settingArray.count;
    }else{
        return _soundArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SettingTableViewCell class])];
        cell.tag = indexPath.row;
        NSString *title = @"게임 방법";
        [cell setTitle:title];
        [cell hideSwitch];
        return cell;
    }else if (indexPath.section == 1) {
        SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SettingTableViewCell class])];
        cell.tag = indexPath.row;
        NSString *title = _settingArray[indexPath.row];
        [cell setTitle:title];
        return cell;
    }else if (indexPath.section == 2) {
        ColorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ColorTableViewCell class])];
        return cell;
    }else if (indexPath.section == 3) {
        SoundTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SoundTableViewCell class])];
        NSString *title = _soundArray[indexPath.row];
        BOOL isCheck = ([SettingManager sharedInstance].selectedSound == indexPath.row);
        [cell setSound:title check:isCheck];
        return cell;
    }else{
        SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SettingTableViewCell class])];
        cell.tag = indexPath.row;
        NSString *title = @"개발자에게 메일보내기";
        [cell setTitle:title];
        [cell hideSwitch];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        GuideViewController *guideViewController = [SharedAppDelegate.mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([GuideViewController class])];
        [self presentViewController:guideViewController animated:YES completion:nil];
    }else if (indexPath.section == 3) {
        [SettingManager sharedInstance].selectedSound = (int)indexPath.row;
        [SettingManager sharedInstance].isOnBGSound = YES;
        [settingTableView reloadData];
        [SharedAppDelegate playBGsound];
    }else if (indexPath.section == 4) {
        [self sendMailToDeveloper];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // 선택된 셀 터치 이벤트 막기.
    if (indexPath.section == 1|| indexPath.section == 2) {
        return nil;
    }else{
        return indexPath;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, 40)];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, DEVICE_WIDTH-20, 40)];
    headerView.backgroundColor = CustomLightGray;
    headerLabel.font = [UIFont systemFontOfSize:23.f];
    [headerView addSubview:headerLabel];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}


#pragma mark - Mail 보내기
- (void)sendMailToDeveloper
{
    // From within your active view controller
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;        // Required to invoke mailComposeController when send
        
        NSString *versionString = [NSString stringWithFormat:@"Version %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
        [mailCont setSubject:[@"[병맛야구]" stringByAppendingString:versionString]];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"yoseop.park86@gmail.com"]];
        [mailCont setMessageBody:@"\n\n---------------------\n\n1. 버그가있어요:\n\n2. 이러한 기능을 추가해주세요:\n\n3. 칭찬:\n\n4. 욕:\n\n5. 자유발언:\n\n---------------------\n\n" isHTML:NO];
        
        [self presentViewController:mailCont animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end

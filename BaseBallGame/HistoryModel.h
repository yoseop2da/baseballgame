//
//  HistoryModel.h
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 3..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HistoryModel : NSObject

@property (strong) NSString *numberValue;

//@property (strong) NSString *matchedValue;

@property (assign) int strike;
@property (assign) int ball;
@end

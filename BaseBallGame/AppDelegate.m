//
//  AppDelegate.m
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 3..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import "AppDelegate.h"

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MediaPlayer/MediaPlayerDefines.h>

@interface AppDelegate ()
{
    MBProgressHUD *hud;
    AVAudioPlayer *_audioPlayer;
    AVAudioPlayer *_effectPlayer;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self playBGsound];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - 프로그래스

- (MBProgressHUD *)showGlobalProgressHUDWithTitle:(NSString *)title {
    UIView *view = self.window.rootViewController.view;
    if (view) {
        [MBProgressHUD hideAllHUDsForView:view animated:YES];
        hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        
        [view bringSubviewToFront:hud];
        [hud setAlpha:0.6];
        [hud hide:YES afterDelay:1.f];

        if (title.length > 0) {
            // Configure for text only and offset down
            hud.mode = MBProgressHUDModeText;
            hud.labelText = title;
            hud.labelFont = [UIFont systemFontOfSize:12];
            hud.margin = 10.f;
            hud.yOffset = 150.f;
        }
        return hud;
    }
    else{
        return nil;
    }
}

- (void)dismissGlobalHUD {
    UIView *view = self.window.rootViewController.view;
    if (view) {
        [MBProgressHUD hideHUDForView:view animated:YES];
    }
}

- (void)playBGsound
{
    if(_audioPlayer && [_audioPlayer isPlaying]){
        [_audioPlayer stop];
    }

    NSString *bgName = [[SettingManager sharedInstance] bgName];
    NSString *path = [[NSBundle mainBundle] pathForResource:bgName ofType:@"mp3"];
    NSURL *file = [NSURL fileURLWithPath:path];
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:file error:nil];
    _audioPlayer.numberOfLoops = -1;
    if ([SettingManager sharedInstance].isOnBGSound) {
        [self stopEffectSound];
        [_audioPlayer play];
    }
}

- (void)stopBGSound
{
    [_audioPlayer stop];
}

- (void)playSuccessSound
{
    [self stopBGSound];
    NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@", @"successSound01"] ofType:@"mp3"];
    [self setEffectSoundWithPath:path];
}

- (void)playBatSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@", @"batSound"] ofType:@"mp3"];
    [self setEffectSoundWithPath:path];
}

- (void)playBallPassSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@", @"ballPassSound"] ofType:@"mp3"];
    [self setEffectSoundWithPath:path];
}

- (void)playOutSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@", @"outSound"] ofType:@"mp3"];
    [self setEffectSoundWithPath:path];
}

- (void)playButtonSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@", @"button01"] ofType:@"mp3"];
    [self setEffectSoundWithPath:path];
}

- (void)setEffectSoundWithPath:(NSString *)path
{
    NSURL *file = [NSURL fileURLWithPath:path];
    _effectPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:file error:nil];
    if ([SettingManager sharedInstance].isOnEffectSound) {
        [_effectPlayer play];
    }
}


- (void)stopEffectSound
{
    [_effectPlayer stop];
}

@end

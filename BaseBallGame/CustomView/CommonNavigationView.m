//
//  CommonNavigationView.m
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 4..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import "CommonNavigationView.h"

@implementation CommonNavigationView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.backgroundColor = NavigationBackGroundColor1;
    }
    return self;
}
@end

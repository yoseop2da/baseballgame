//
//  HintView.m
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 4..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import "HintView.h"
@interface HintView()
{
    int resultIndex;
}
@end

@implementation HintView

-(instancetype)initWithHintArray:(NSArray *)resultArray
{
    if (self = [super init]) {
        self.frame = CGRectMake(-1000, -1000, 200, 200);
        self.clipsToBounds = YES;
        self.layer.cornerRadius = 100.f;
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
        textLabel.text = [resultArray componentsJoinedByString:@""];;
        textLabel.font = [UIFont systemFontOfSize:25];
        textLabel.textAlignment = NSTextAlignmentCenter;
        
        UIImage *image = [UIImage imageNamed:@"ball"];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
        imageView.image = image;
//        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:imageView];
        [self addSubview:textLabel];
        [self.subviews.firstObject bringSubviewToFront:self];
    }
    return self;
}

- (void)showHintView
{
    __block int xValue = ((int)arc4random_uniform(10)+1)*100;
    if (xValue < 500) {
        xValue += 500;
    }
    __block int yValue = ((int)arc4random_uniform(10)+1)*100;
    if (yValue < 500) {
        yValue += 500;
    }
    
    
    switch (resultIndex %4) {
        case 0:
        {
            xValue *= (1);
            yValue *= (1);
        }
            break;
        case 1:
        {
            xValue *= (1);
            yValue *= (-1);
        }
            break;
        case 2:
        {
            xValue *= (-1);
            yValue *= (1);
        }
            break;
        case 3:
        {
            xValue *= (-1);
            yValue *= (-1);
        }
            break;
        default:
            break;
    }
    
    self.center = CGPointMake(xValue, yValue);
    
    [UIView animateWithDuration:1.5f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        self.center = CGPointMake(xValue*(-1) + width, yValue*(-1) + height);
    }completion:nil];
    
    [self runSpinAnimationOnView:self duration:10.5f rotations:2.f repeat:10];
    resultIndex++;
}

- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

@end

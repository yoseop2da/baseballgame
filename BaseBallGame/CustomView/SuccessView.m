//
//  SuccessView.m
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 4..
//  Copyright © 2015년 apposter. All rights reserved.
//


#define Dark [UIColor colorWithRed:(38/255.f) green:(41/255.f) blue:(40/255.f) alpha:1]

#define Yellow [UIColor colorWithRed:(255/255.f) green:(194/255.f) blue:(54/255.f) alpha:1]

#import "SuccessView.h"
@interface SuccessView()
{
    int index;
    UIView *_containerView;
}
@end

@implementation SuccessView

-(instancetype)init
{
    if (self = [super init]) {
        self.frame = CGRectMake(0, 0, SharedAppDelegate.window.frame.size.width, SharedAppDelegate.window.frame.size.height);
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.1];
        self.clipsToBounds = YES;
        
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
        textLabel.text = @"Home Run~ !!";
        textLabel.font = [UIFont systemFontOfSize:20];
        textLabel.textAlignment = NSTextAlignmentCenter;
        
        UIImage *image = [UIImage imageNamed:@"ball"];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
        imageView.image = image;
        
        UIButton *newGameButton = [[UIButton alloc] initWithFrame:CGRectMake(40, 140, 120, 30)];
        [newGameButton setTitleColor:Yellow forState:UIControlStateNormal];
        [newGameButton setTitle:@"New Game?" forState:UIControlStateNormal];
        newGameButton.backgroundColor = Dark;
        newGameButton.layer.cornerRadius = 10;
        
        [newGameButton addTarget:self action:@selector(newGameButtonTouched) forControlEvents:UIControlEventTouchUpInside];
        
        [_containerView addSubview:imageView];
        [_containerView addSubview:textLabel];
        [_containerView addSubview:newGameButton];
        [self addSubview:_containerView];
        
        CGPoint center = SharedAppDelegate.window.center;
        self.center = center;
        _containerView.center = center;
    }
    return self;
}

- (void)animating
{
    [SharedAppDelegate playSuccessSound];
    self.hidden = NO;
    [UIView animateWithDuration:.3f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
        _containerView.center = CGPointMake(self.center.x+20, self.center.y+20);
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:.3f delay:0.f options:UIViewAnimationOptionCurveEaseOut animations:^{
            _containerView.center = CGPointMake(self.center.x-10, self.center.y-10);
        }completion:^(BOOL finished) {
            
        }];
    }];

}

- (void)newGameButtonTouched
{
    [SharedAppDelegate stopEffectSound];
    [SharedAppDelegate playBGsound];
    self.hidden = YES;
    [SharedAppDelegate.mainViewController initialzeRandomNumber];
    [SharedAppDelegate.mainViewController loadFullAd];
}
@end

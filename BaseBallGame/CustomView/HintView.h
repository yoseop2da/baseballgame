//
//  HintView.h
//  BaseBallGame
//
//  Created by yoseop on 2015. 11. 4..
//  Copyright © 2015년 apposter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HintView : UIView

-(instancetype)initWithHintArray:(NSArray *)resultArray;

- (void)showHintView;

@end
